(defdomain domain (
  (:operator (!drive ?v ?l1 ?l2)
    ;; preconditions
    (
      (type-vehicle ?v) (type-location ?l1) (type-location ?l2)
      (at ?v ?l1) (road ?l1 ?l2)
    )
    ;; delete effects
    ((at ?v ?l1))
    ;; add effects
    ((at ?v ?l2))
  )
  (:operator (!noop ?v ?l2)
    ;; preconditions
    (
      (type-vehicle ?v) (type-location ?l2)
      (at ?v ?l2)
    )
    ;; delete effects
    ()
    ;; add effects
    ()
  )
  (:operator (!pick-up ?v ?l ?p ?s1 ?s2)
    ;; preconditions
    (
      (type-vehicle ?v) (type-location ?l) (type-package ?p) (type-capacity-number ?s1) (type-capacity-number ?s2)
      (at ?v ?l) (at ?p ?l) (capacity-predecessor ?s1 ?s2) (capacity ?v ?s2)
    )
    ;; delete effects
    ((at ?p ?l) (capacity ?v ?s2))
    ;; add effects
    ((in ?p ?v) (capacity ?v ?s1))
  )
  (:operator (!drop ?v ?l ?p ?s1 ?s2)
    ;; preconditions
    (
      (type-vehicle ?v) (type-location ?l) (type-package ?p) (type-capacity-number ?s1) (type-capacity-number ?s2)
      (at ?v ?l) (in ?p ?v) (capacity-predecessor ?s1 ?s2) (capacity ?v ?s1)
    )
    ;; delete effects
    ((in ?p ?v) (capacity ?v ?s1))
    ;; add effects
    ((at ?p ?l) (capacity ?v ?s2))
  )
  (:method (x--top)
    x--top-method
    (
      
      (type-sort-for-city-loc-8 ?var-for-city-loc-8-1) (type-sort-for-package-0 ?var-for-package-0-2) (type-sort-for-city-loc-8 ?var-for-city-loc-8-3) (type-sort-for-package-1 ?var-for-package-1-4) (type-sort-for-city-loc-8 ?var-for-city-loc-8-5) (type-sort-for-package-2 ?var-for-package-2-6) (type-sort-for-city-loc-5 ?var-for-city-loc-5-7) (type-sort-for-package-3 ?var-for-package-3-8) (type-sort-for-city-loc-8 ?var-for-city-loc-8-9) (type-sort-for-package-4 ?var-for-package-4-10) (type-sort-for-city-loc-4 ?var-for-city-loc-4-11) (type-sort-for-package-5 ?var-for-package-5-12) (type-sort-for-city-loc-6 ?var-for-city-loc-6-13) (type-sort-for-package-6 ?var-for-package-6-14) (type-sort-for-city-loc-0 ?var-for-city-loc-0-15) (type-sort-for-package-7 ?var-for-package-7-16) (type-sort-for-city-loc-5 ?var-for-city-loc-5-17) (type-sort-for-package-8 ?var-for-package-8-18)
      
    )
    ((deliver ?var-for-package-4-10 ?var-for-city-loc-8-9) (deliver ?var-for-package-0-2 ?var-for-city-loc-8-1) (deliver ?var-for-package-3-8 ?var-for-city-loc-5-7) (deliver ?var-for-package-8-18 ?var-for-city-loc-5-17) (deliver ?var-for-package-6-14 ?var-for-city-loc-6-13) (deliver ?var-for-package-2-6 ?var-for-city-loc-8-5) (deliver ?var-for-package-7-16 ?var-for-city-loc-0-15) (deliver ?var-for-package-5-12 ?var-for-city-loc-4-11) (deliver ?var-for-package-1-4 ?var-for-city-loc-8-3))
  )
  (:method (deliver ?p ?l2)
    m-deliver-ordering-0
    (
      (type-package ?p) (type-location ?l2)
      (type-location ?l1) (type-location ?l2) (type-package ?p) (type-vehicle ?v)
      
    )
    ((get-to ?v ?l1) (load ?v ?l1 ?p) (get-to ?v ?l2) (unload ?v ?l2 ?p))
  )
  (:method (get-to ?v ?l2)
    m-drive-to-ordering-0
    (
      (type-vehicle ?v) (type-location ?l2)
      (type-location ?l1) (type-location ?l2) (type-vehicle ?v)
      
    )
    ((!drive ?v ?l1 ?l2))
  )
  (:method (get-to ?v ?l3)
    m-drive-to-via-ordering-0
    (
      (type-vehicle ?v) (type-location ?l3)
      (type-location ?l2) (type-location ?l3) (type-vehicle ?v)
      
    )
    ((get-to ?v ?l2) (!drive ?v ?l2 ?l3))
  )
  (:method (get-to ?v ?l)
    m-i-am-there-ordering-0
    (
      (type-vehicle ?v) (type-location ?l)
      (type-location ?l) (type-vehicle ?v)
      
    )
    ((!noop ?v ?l))
  )
  (:method (load ?v ?l ?p)
    m-load-ordering-0
    (
      (type-vehicle ?v) (type-location ?l) (type-package ?p)
      (type-location ?l) (type-package ?p) (type-capacity-number ?s1) (type-capacity-number ?s2) (type-vehicle ?v)
      
    )
    ((!pick-up ?v ?l ?p ?s1 ?s2))
  )
  (:method (unload ?v ?l ?p)
    m-unload-ordering-0
    (
      (type-vehicle ?v) (type-location ?l) (type-package ?p)
      (type-location ?l) (type-package ?p) (type-capacity-number ?s1) (type-capacity-number ?s2) (type-vehicle ?v)
      
    )
    ((!drop ?v ?l ?p ?s1 ?s2))
  )
))
