(defdomain domain (
  (:operator (!navigate ?x ?y ?z)
    ;; preconditions
    (
      (type-rover ?x) (type-waypoint ?y) (type-waypoint ?z)
      (can-traverse ?x ?y ?z) (available ?x) (at ?x ?y) (visible ?y ?z)
    )
    ;; delete effects
    ((at ?x ?y))
    ;; add effects
    ((at ?x ?z))
  )
  (:operator (!sample-soil ?x ?s ?p)
    ;; preconditions
    (
      (type-rover ?x) (type-store ?s) (type-waypoint ?p)
      (at ?x ?p) (at-soil-sample ?p) (equipped-for-soil-analysis ?x) (store-of ?s ?x) (empty ?s)
    )
    ;; delete effects
    ((empty ?s) (at-soil-sample ?p))
    ;; add effects
    ((full ?s) (have-soil-analysis ?x ?p))
  )
  (:operator (!sample-rock ?x ?s ?p)
    ;; preconditions
    (
      (type-rover ?x) (type-store ?s) (type-waypoint ?p)
      (at-rock-sample ?p) (equipped-for-rock-analysis ?x) (store-of ?s ?x) (empty ?s)
    )
    ;; delete effects
    ((empty ?s) (at-rock-sample ?p))
    ;; add effects
    ((full ?s) (have-rock-analysis ?x ?p))
  )
  (:operator (!drop ?x ?y)
    ;; preconditions
    (
      (type-rover ?x) (type-store ?y)
      (store-of ?y ?x) (full ?y)
    )
    ;; delete effects
    ((full ?y))
    ;; add effects
    ((empty ?y))
  )
  (:operator (!calibrate ?r ?i ?t ?w)
    ;; preconditions
    (
      (type-rover ?r) (type-camera ?i) (type-objective ?t) (type-waypoint ?w)
      (equipped-for-imaging ?r) (calibration-target ?i ?t) (at ?r ?w) (visible-from ?t ?w) (on-board ?i ?r)
    )
    ;; delete effects
    ()
    ;; add effects
    ((calibrated ?i ?r))
  )
  (:operator (!take-image ?r ?p ?o ?i ?m)
    ;; preconditions
    (
      (type-rover ?r) (type-waypoint ?p) (type-objective ?o) (type-camera ?i) (type-mode ?m)
      (calibrated ?i ?r) (on-board ?i ?r) (equipped-for-imaging ?r) (supports ?i ?m) (visible-from ?o ?p) (at ?r ?p)
    )
    ;; delete effects
    ((calibrated ?i ?r))
    ;; add effects
    ((have-image ?r ?o ?m))
  )
  (:operator (!communicate-soil-data ?r ?l ?p ?x ?y)
    ;; preconditions
    (
      (type-rover ?r) (type-lander ?l) (type-waypoint ?p) (type-waypoint ?x) (type-waypoint ?y)
      (at ?r ?x) (at-lander ?l ?y) (have-soil-analysis ?r ?p) (visible ?x ?y) (available ?r) (channel-free ?l)
    )
    ;; delete effects
    ((available ?r) (channel-free ?l))
    ;; add effects
    ((channel-free ?l) (communicated-soil-data ?p) (available ?r))
  )
  (:operator (!communicate-rock-data ?r ?l ?p ?x ?y)
    ;; preconditions
    (
      (type-rover ?r) (type-lander ?l) (type-waypoint ?p) (type-waypoint ?x) (type-waypoint ?y)
      (at ?r ?x) (at-lander ?l ?y) (have-rock-analysis ?r ?p) (visible ?x ?y) (available ?r) (channel-free ?l)
    )
    ;; delete effects
    ((available ?r) (channel-free ?l))
    ;; add effects
    ((channel-free ?l) (communicated-rock-data ?p) (available ?r))
  )
  (:operator (!communicate-image-data ?r ?l ?o ?m ?x ?y)
    ;; preconditions
    (
      (type-rover ?r) (type-lander ?l) (type-objective ?o) (type-mode ?m) (type-waypoint ?x) (type-waypoint ?y)
      (at ?r ?x) (at-lander ?l ?y) (have-image ?r ?o ?m) (visible ?x ?y) (available ?r) (channel-free ?l)
    )
    ;; delete effects
    ((available ?r) (channel-free ?l))
    ;; add effects
    ((channel-free ?l) (communicated-image-data ?o ?m) (available ?r))
  )
  (:operator (!visit ?waypoint)
    ;; preconditions
    (
      (type-waypoint ?waypoint)
      
    )
    ;; delete effects
    ()
    ;; add effects
    ((visited ?waypoint))
  )
  (:operator (!unvisit ?waypoint)
    ;; preconditions
    (
      (type-waypoint ?waypoint)
      
    )
    ;; delete effects
    ((visited ?waypoint))
    ;; add effects
    ()
  )
  (:method (x--top)
    x--top-method
    (
      
      (type-sort-for-waypoint8 ?var-for-waypoint8-1) (type-sort-for-waypoint23 ?var-for-waypoint23-2) (type-sort-for-waypoint7 ?var-for-waypoint7-3) (type-sort-for-waypoint11 ?var-for-waypoint11-4) (type-sort-for-waypoint13 ?var-for-waypoint13-5) (type-sort-for-waypoint20 ?var-for-waypoint20-6) (type-sort-for-waypoint7 ?var-for-waypoint7-7) (type-sort-for-waypoint14 ?var-for-waypoint14-8) (type-sort-for-waypoint22 ?var-for-waypoint22-9) (type-sort-for-waypoint16 ?var-for-waypoint16-10) (type-sort-for-waypoint12 ?var-for-waypoint12-11) (type-sort-for-waypoint10 ?var-for-waypoint10-12) (type-sort-for-waypoint9 ?var-for-waypoint9-13) (type-sort-for-waypoint21 ?var-for-waypoint21-14) (type-sort-for-waypoint18 ?var-for-waypoint18-15) (type-sort-for-high-res ?var-for-high-res-16) (type-sort-for-objective2 ?var-for-objective2-17) (type-sort-for-high-res ?var-for-high-res-18) (type-sort-for-objective0 ?var-for-objective0-19) (type-sort-for-colour ?var-for-colour-20) (type-sort-for-objective3 ?var-for-objective3-21) (type-sort-for-colour ?var-for-colour-22) (type-sort-for-objective7 ?var-for-objective7-23) (type-sort-for-high-res ?var-for-high-res-24) (type-sort-for-objective5 ?var-for-objective5-25)
      
    )
    ((get-rock-data ?var-for-waypoint18-15) (get-image-data ?var-for-objective0-19 ?var-for-high-res-18) (get-rock-data ?var-for-waypoint22-9) (get-soil-data ?var-for-waypoint11-4) (get-soil-data ?var-for-waypoint23-2) (get-image-data ?var-for-objective5-25 ?var-for-high-res-24) (get-rock-data ?var-for-waypoint21-14) (get-image-data ?var-for-objective3-21 ?var-for-colour-20) (get-rock-data ?var-for-waypoint16-10) (get-image-data ?var-for-objective7-23 ?var-for-colour-22) (get-soil-data ?var-for-waypoint8-1) (get-soil-data ?var-for-waypoint13-5) (get-rock-data ?var-for-waypoint9-13) (get-soil-data ?var-for-waypoint7-3) (get-rock-data ?var-for-waypoint12-11) (get-rock-data ?var-for-waypoint7-7) (get-image-data ?var-for-objective2-17 ?var-for-high-res-16) (get-soil-data ?var-for-waypoint20-6) (get-rock-data ?var-for-waypoint14-8) (get-rock-data ?var-for-waypoint10-12))
  )
  (:method (calibrate-abs ?rover ?camera)
    m-calibrate-abs-ordering-0
    (
      (type-rover ?rover) (type-camera ?camera)
      (type-camera ?camera) (type-objective ?objective) (type-rover ?rover) (type-waypoint ?waypoint)
      (calibration-target ?camera ?objective) (visible-from ?objective ?waypoint)
    )
    ((navigate-abs ?rover ?waypoint) (!calibrate ?rover ?camera ?objective ?waypoint))
  )
  (:method (empty-store ?s ?rover)
    m-empty-store-1-ordering-0
    (
      (type-store ?s) (type-rover ?rover)
      (type-rover ?rover) (type-store ?s)
      (empty ?s)
    )
    ()
  )
  (:method (empty-store ?s ?rover)
    m-empty-store-2-ordering-0
    (
      (type-store ?s) (type-rover ?rover)
      (type-rover ?rover) (type-store ?s)
      (not (empty ?s))
    )
    ((!drop ?rover ?s))
  )
  (:method (get-image-data ?objective ?mode)
    m-get-image-data-ordering-0
    (
      (type-objective ?objective) (type-mode ?mode)
      (type-camera ?camera) (type-mode ?mode) (type-objective ?objective) (type-rover ?rover) (type-waypoint ?waypoint)
      (equipped-for-imaging ?rover) (on-board ?camera ?rover) (supports ?camera ?mode) (visible-from ?objective ?waypoint)
    )
    ((calibrate-abs ?rover ?camera) (navigate-abs ?rover ?waypoint) (!take-image ?rover ?waypoint ?objective ?camera ?mode) (send-image-data ?rover ?objective ?mode))
  )
  (:method (get-rock-data ?waypoint)
    m-get-rock-data-ordering-0
    (
      (type-waypoint ?waypoint)
      (type-rover ?rover) (type-store ?s) (type-waypoint ?waypoint)
      (equipped-for-rock-analysis ?rover) (store-of ?s ?rover)
    )
    ((navigate-abs ?rover ?waypoint) (empty-store ?s ?rover) (!sample-rock ?rover ?s ?waypoint) (send-rock-data ?rover ?waypoint))
  )
  (:method (get-soil-data ?waypoint)
    m-get-soil-data-ordering-0
    (
      (type-waypoint ?waypoint)
      (type-rover ?rover) (type-store ?s) (type-waypoint ?waypoint)
      (store-of ?s ?rover) (equipped-for-soil-analysis ?rover)
    )
    ((navigate-abs ?rover ?waypoint) (empty-store ?s ?rover) (!sample-soil ?rover ?s ?waypoint) (send-soil-data ?rover ?waypoint))
  )
  (:method (navigate-abs ?rover ?to)
    m-navigate-abs-1-ordering-0
    (
      (type-rover ?rover) (type-waypoint ?to)
      (type-waypoint ?from) (type-rover ?rover) (type-waypoint ?to)
      (at ?rover ?from)
    )
    ((!visit ?from) (!navigate ?rover ?from ?to) (!unvisit ?from))
  )
  (:method (navigate-abs ?rover ?to)
    m-navigate-abs-2-ordering-0
    (
      (type-rover ?rover) (type-waypoint ?to)
      (type-rover ?rover) (type-waypoint ?to)
      (at ?rover ?to)
    )
    ()
  )
  (:method (navigate-abs ?rover ?to)
    m-navigate-abs-3-ordering-0
    (
      (type-rover ?rover) (type-waypoint ?to)
      (type-waypoint ?from) (type-rover ?rover) (type-waypoint ?to)
      (not (at ?rover ?to)) (can-traverse ?rover ?from ?to)
    )
    ((!navigate ?rover ?from ?to))
  )
  (:method (navigate-abs ?rover ?to)
    m-navigate-abs-4-ordering-0
    (
      (type-rover ?rover) (type-waypoint ?to)
      (type-waypoint ?from) (type-waypoint ?mid) (type-rover ?rover) (type-waypoint ?to)
      (not (at ?rover ?to)) (not (can-traverse ?rover ?from ?to)) (can-traverse ?rover ?from ?mid) (not (visited ?mid))
    )
    ((!navigate ?rover ?from ?mid) (!visit ?mid) (!navigate ?rover ?mid ?to) (!unvisit ?mid))
  )
  (:method (send-image-data ?rover ?objective ?mode)
    m-send-image-data-ordering-0
    (
      (type-rover ?rover) (type-objective ?objective) (type-mode ?mode)
      (type-lander ?l) (type-mode ?mode) (type-objective ?objective) (type-rover ?rover) (type-waypoint ?x) (type-waypoint ?y)
      (at-lander ?l ?y) (visible ?x ?y)
    )
    ((navigate-abs ?rover ?x) (!communicate-image-data ?rover ?l ?objective ?mode ?x ?y))
  )
  (:method (send-rock-data ?rover ?waypoint)
    m-send-rock-data-ordering-0
    (
      (type-rover ?rover) (type-waypoint ?waypoint)
      (type-lander ?l) (type-rover ?rover) (type-waypoint ?waypoint) (type-waypoint ?x) (type-waypoint ?y)
      (at-lander ?l ?y) (visible ?x ?y)
    )
    ((navigate-abs ?rover ?x) (!communicate-rock-data ?rover ?l ?waypoint ?x ?y))
  )
  (:method (send-soil-data ?rover ?waypoint)
    m-send-soil-data-ordering-0
    (
      (type-rover ?rover) (type-waypoint ?waypoint)
      (type-lander ?l) (type-rover ?rover) (type-waypoint ?waypoint) (type-waypoint ?x) (type-waypoint ?y)
      (at-lander ?l ?y) (visible ?x ?y)
    )
    ((navigate-abs ?rover ?x) (!communicate-soil-data ?rover ?l ?waypoint ?x ?y))
  )
))
