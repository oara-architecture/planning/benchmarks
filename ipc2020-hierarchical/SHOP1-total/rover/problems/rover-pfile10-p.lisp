(defproblem problem domain 
  (
    (visible waypoint0 waypoint6)
    (visible waypoint6 waypoint0)
    (visible waypoint1 waypoint0)
    (visible waypoint0 waypoint1)
    (visible waypoint1 waypoint2)
    (visible waypoint2 waypoint1)
    (visible waypoint1 waypoint5)
    (visible waypoint5 waypoint1)
    (visible waypoint2 waypoint0)
    (visible waypoint0 waypoint2)
    (visible waypoint2 waypoint4)
    (visible waypoint4 waypoint2)
    (visible waypoint2 waypoint6)
    (visible waypoint6 waypoint2)
    (visible waypoint3 waypoint5)
    (visible waypoint5 waypoint3)
    (visible waypoint4 waypoint0)
    (visible waypoint0 waypoint4)
    (visible waypoint4 waypoint1)
    (visible waypoint1 waypoint4)
    (visible waypoint4 waypoint3)
    (visible waypoint3 waypoint4)
    (visible waypoint5 waypoint0)
    (visible waypoint0 waypoint5)
    (visible waypoint5 waypoint6)
    (visible waypoint6 waypoint5)
    (visible waypoint6 waypoint4)
    (visible waypoint4 waypoint6)
    (at-soil-sample waypoint0)
    (at-rock-sample waypoint0)
    (at-rock-sample waypoint1)
    (at-soil-sample waypoint3)
    (at-rock-sample waypoint3)
    (at-soil-sample waypoint4)
    (at-rock-sample waypoint4)
    (at-soil-sample waypoint6)
    (at-rock-sample waypoint6)
    (at-lander general waypoint1)
    (channel-free general)
    (at rover0 waypoint4)
    (available rover0)
    (store-of rover0store rover0)
    (empty rover0store)
    (equipped-for-soil-analysis rover0)
    (equipped-for-rock-analysis rover0)
    (can-traverse rover0 waypoint4 waypoint0)
    (can-traverse rover0 waypoint0 waypoint4)
    (can-traverse rover0 waypoint4 waypoint1)
    (can-traverse rover0 waypoint1 waypoint4)
    (can-traverse rover0 waypoint4 waypoint2)
    (can-traverse rover0 waypoint2 waypoint4)
    (can-traverse rover0 waypoint4 waypoint3)
    (can-traverse rover0 waypoint3 waypoint4)
    (can-traverse rover0 waypoint4 waypoint6)
    (can-traverse rover0 waypoint6 waypoint4)
    (can-traverse rover0 waypoint1 waypoint5)
    (can-traverse rover0 waypoint5 waypoint1)
    (at rover1 waypoint0)
    (available rover1)
    (store-of rover1store rover1)
    (empty rover1store)
    (equipped-for-soil-analysis rover1)
    (equipped-for-imaging rover1)
    (can-traverse rover1 waypoint0 waypoint1)
    (can-traverse rover1 waypoint1 waypoint0)
    (can-traverse rover1 waypoint0 waypoint2)
    (can-traverse rover1 waypoint2 waypoint0)
    (can-traverse rover1 waypoint0 waypoint6)
    (can-traverse rover1 waypoint6 waypoint0)
    (at rover2 waypoint3)
    (available rover2)
    (store-of rover2store rover2)
    (empty rover2store)
    (equipped-for-rock-analysis rover2)
    (equipped-for-imaging rover2)
    (can-traverse rover2 waypoint3 waypoint4)
    (can-traverse rover2 waypoint4 waypoint3)
    (can-traverse rover2 waypoint3 waypoint5)
    (can-traverse rover2 waypoint5 waypoint3)
    (can-traverse rover2 waypoint4 waypoint0)
    (can-traverse rover2 waypoint0 waypoint4)
    (can-traverse rover2 waypoint4 waypoint1)
    (can-traverse rover2 waypoint1 waypoint4)
    (can-traverse rover2 waypoint4 waypoint2)
    (can-traverse rover2 waypoint2 waypoint4)
    (at rover3 waypoint1)
    (available rover3)
    (store-of rover3store rover3)
    (empty rover3store)
    (equipped-for-soil-analysis rover3)
    (equipped-for-rock-analysis rover3)
    (equipped-for-imaging rover3)
    (can-traverse rover3 waypoint1 waypoint0)
    (can-traverse rover3 waypoint0 waypoint1)
    (can-traverse rover3 waypoint0 waypoint2)
    (can-traverse rover3 waypoint2 waypoint0)
    (can-traverse rover3 waypoint0 waypoint4)
    (can-traverse rover3 waypoint4 waypoint0)
    (can-traverse rover3 waypoint0 waypoint6)
    (can-traverse rover3 waypoint6 waypoint0)
    (can-traverse rover3 waypoint4 waypoint3)
    (can-traverse rover3 waypoint3 waypoint4)
    (can-traverse rover3 waypoint6 waypoint5)
    (can-traverse rover3 waypoint5 waypoint6)
    (on-board camera0 rover1)
    (calibration-target camera0 objective2)
    (supports camera0 low-res)
    (on-board camera1 rover1)
    (calibration-target camera1 objective3)
    (supports camera1 colour)
    (on-board camera2 rover1)
    (calibration-target camera2 objective1)
    (supports camera2 colour)
    (supports camera2 low-res)
    (on-board camera3 rover1)
    (calibration-target camera3 objective2)
    (supports camera3 high-res)
    (supports camera3 low-res)
    (on-board camera4 rover2)
    (calibration-target camera4 objective0)
    (supports camera4 colour)
    (on-board camera5 rover3)
    (calibration-target camera5 objective0)
    (supports camera5 colour)
    (supports camera5 high-res)
    (supports camera5 low-res)
    (visible-from objective0 waypoint0)
    (visible-from objective1 waypoint0)
    (visible-from objective1 waypoint1)
    (visible-from objective1 waypoint2)
    (visible-from objective1 waypoint3)
    (visible-from objective2 waypoint0)
    (visible-from objective2 waypoint1)
    (visible-from objective2 waypoint2)
    (visible-from objective2 waypoint3)
    (visible-from objective3 waypoint0)
    (visible-from objective3 waypoint1)
    (visible-from objective3 waypoint2)
    (visible-from objective3 waypoint3)
    (visible-from objective3 waypoint4)
    (visible-from objective3 waypoint5)
    (type-camera camera0)
    (type-camera camera1)
    (type-camera camera2)
    (type-camera camera3)
    (type-camera camera4)
    (type-camera camera5)
    (type-lander general)
    (type-mode colour)
    (type-mode high-res)
    (type-mode low-res)
    (type-object camera0)
    (type-object camera1)
    (type-object camera2)
    (type-object camera3)
    (type-object camera4)
    (type-object camera5)
    (type-object colour)
    (type-object general)
    (type-object high-res)
    (type-object low-res)
    (type-object objective0)
    (type-object objective1)
    (type-object objective2)
    (type-object objective3)
    (type-object rover0)
    (type-object rover0store)
    (type-object rover1)
    (type-object rover1store)
    (type-object rover2)
    (type-object rover2store)
    (type-object rover3)
    (type-object rover3store)
    (type-object waypoint0)
    (type-object waypoint1)
    (type-object waypoint2)
    (type-object waypoint3)
    (type-object waypoint4)
    (type-object waypoint5)
    (type-object waypoint6)
    (type-objective objective0)
    (type-objective objective1)
    (type-objective objective2)
    (type-objective objective3)
    (type-rover rover0)
    (type-rover rover1)
    (type-rover rover2)
    (type-rover rover3)
    (type-sort-for-camera0 camera0)
    (type-sort-for-camera1 camera1)
    (type-sort-for-camera2 camera2)
    (type-sort-for-camera3 camera3)
    (type-sort-for-camera4 camera4)
    (type-sort-for-camera5 camera5)
    (type-sort-for-colour colour)
    (type-sort-for-general general)
    (type-sort-for-high-res high-res)
    (type-sort-for-low-res low-res)
    (type-sort-for-objective0 objective0)
    (type-sort-for-objective1 objective1)
    (type-sort-for-objective2 objective2)
    (type-sort-for-objective3 objective3)
    (type-sort-for-rover0 rover0)
    (type-sort-for-rover0store rover0store)
    (type-sort-for-rover1 rover1)
    (type-sort-for-rover1store rover1store)
    (type-sort-for-rover2 rover2)
    (type-sort-for-rover2store rover2store)
    (type-sort-for-rover3 rover3)
    (type-sort-for-rover3store rover3store)
    (type-sort-for-waypoint0 waypoint0)
    (type-sort-for-waypoint1 waypoint1)
    (type-sort-for-waypoint2 waypoint2)
    (type-sort-for-waypoint3 waypoint3)
    (type-sort-for-waypoint4 waypoint4)
    (type-sort-for-waypoint5 waypoint5)
    (type-sort-for-waypoint6 waypoint6)
    (type-store rover0store)
    (type-store rover1store)
    (type-store rover2store)
    (type-store rover3store)
    (type-waypoint waypoint0)
    (type-waypoint waypoint1)
    (type-waypoint waypoint2)
    (type-waypoint waypoint3)
    (type-waypoint waypoint4)
    (type-waypoint waypoint5)
    (type-waypoint waypoint6)
  )
  ((x--top))
)
