(defproblem problem domain 
  (
    (visible waypoint0 waypoint10)
    (visible waypoint10 waypoint0)
    (visible waypoint0 waypoint11)
    (visible waypoint11 waypoint0)
    (visible waypoint1 waypoint0)
    (visible waypoint0 waypoint1)
    (visible waypoint1 waypoint3)
    (visible waypoint3 waypoint1)
    (visible waypoint1 waypoint10)
    (visible waypoint10 waypoint1)
    (visible waypoint2 waypoint4)
    (visible waypoint4 waypoint2)
    (visible waypoint2 waypoint5)
    (visible waypoint5 waypoint2)
    (visible waypoint2 waypoint9)
    (visible waypoint9 waypoint2)
    (visible waypoint2 waypoint10)
    (visible waypoint10 waypoint2)
    (visible waypoint3 waypoint2)
    (visible waypoint2 waypoint3)
    (visible waypoint3 waypoint4)
    (visible waypoint4 waypoint3)
    (visible waypoint3 waypoint5)
    (visible waypoint5 waypoint3)
    (visible waypoint3 waypoint8)
    (visible waypoint8 waypoint3)
    (visible waypoint3 waypoint9)
    (visible waypoint9 waypoint3)
    (visible waypoint3 waypoint10)
    (visible waypoint10 waypoint3)
    (visible waypoint4 waypoint5)
    (visible waypoint5 waypoint4)
    (visible waypoint4 waypoint7)
    (visible waypoint7 waypoint4)
    (visible waypoint4 waypoint11)
    (visible waypoint11 waypoint4)
    (visible waypoint6 waypoint1)
    (visible waypoint1 waypoint6)
    (visible waypoint6 waypoint2)
    (visible waypoint2 waypoint6)
    (visible waypoint6 waypoint8)
    (visible waypoint8 waypoint6)
    (visible waypoint7 waypoint0)
    (visible waypoint0 waypoint7)
    (visible waypoint7 waypoint5)
    (visible waypoint5 waypoint7)
    (visible waypoint7 waypoint6)
    (visible waypoint6 waypoint7)
    (visible waypoint7 waypoint10)
    (visible waypoint10 waypoint7)
    (visible waypoint8 waypoint0)
    (visible waypoint0 waypoint8)
    (visible waypoint8 waypoint5)
    (visible waypoint5 waypoint8)
    (visible waypoint8 waypoint7)
    (visible waypoint7 waypoint8)
    (visible waypoint8 waypoint10)
    (visible waypoint10 waypoint8)
    (visible waypoint9 waypoint0)
    (visible waypoint0 waypoint9)
    (visible waypoint9 waypoint1)
    (visible waypoint1 waypoint9)
    (visible waypoint9 waypoint4)
    (visible waypoint4 waypoint9)
    (visible waypoint10 waypoint5)
    (visible waypoint5 waypoint10)
    (visible waypoint10 waypoint9)
    (visible waypoint9 waypoint10)
    (visible waypoint11 waypoint7)
    (visible waypoint7 waypoint11)
    (visible waypoint11 waypoint8)
    (visible waypoint8 waypoint11)
    (visible waypoint11 waypoint9)
    (visible waypoint9 waypoint11)
    (visible waypoint11 waypoint10)
    (visible waypoint10 waypoint11)
    (at-soil-sample waypoint0)
    (at-soil-sample waypoint1)
    (at-rock-sample waypoint1)
    (at-rock-sample waypoint2)
    (at-rock-sample waypoint3)
    (at-soil-sample waypoint4)
    (at-soil-sample waypoint5)
    (at-rock-sample waypoint5)
    (at-soil-sample waypoint7)
    (at-soil-sample waypoint8)
    (at-rock-sample waypoint8)
    (at-soil-sample waypoint9)
    (at-rock-sample waypoint9)
    (at-rock-sample waypoint10)
    (at-soil-sample waypoint11)
    (at-rock-sample waypoint11)
    (at-lander general waypoint6)
    (channel-free general)
    (at rover0 waypoint6)
    (available rover0)
    (store-of rover0store rover0)
    (empty rover0store)
    (equipped-for-soil-analysis rover0)
    (equipped-for-rock-analysis rover0)
    (equipped-for-imaging rover0)
    (can-traverse rover0 waypoint6 waypoint1)
    (can-traverse rover0 waypoint1 waypoint6)
    (can-traverse rover0 waypoint6 waypoint2)
    (can-traverse rover0 waypoint2 waypoint6)
    (can-traverse rover0 waypoint6 waypoint7)
    (can-traverse rover0 waypoint7 waypoint6)
    (can-traverse rover0 waypoint1 waypoint0)
    (can-traverse rover0 waypoint0 waypoint1)
    (can-traverse rover0 waypoint1 waypoint3)
    (can-traverse rover0 waypoint3 waypoint1)
    (can-traverse rover0 waypoint1 waypoint9)
    (can-traverse rover0 waypoint9 waypoint1)
    (can-traverse rover0 waypoint2 waypoint5)
    (can-traverse rover0 waypoint5 waypoint2)
    (can-traverse rover0 waypoint7 waypoint4)
    (can-traverse rover0 waypoint4 waypoint7)
    (can-traverse rover0 waypoint7 waypoint8)
    (can-traverse rover0 waypoint8 waypoint7)
    (can-traverse rover0 waypoint7 waypoint10)
    (can-traverse rover0 waypoint10 waypoint7)
    (can-traverse rover0 waypoint7 waypoint11)
    (can-traverse rover0 waypoint11 waypoint7)
    (at rover1 waypoint2)
    (available rover1)
    (store-of rover1store rover1)
    (empty rover1store)
    (equipped-for-soil-analysis rover1)
    (equipped-for-rock-analysis rover1)
    (can-traverse rover1 waypoint2 waypoint3)
    (can-traverse rover1 waypoint3 waypoint2)
    (can-traverse rover1 waypoint2 waypoint4)
    (can-traverse rover1 waypoint4 waypoint2)
    (can-traverse rover1 waypoint2 waypoint5)
    (can-traverse rover1 waypoint5 waypoint2)
    (can-traverse rover1 waypoint2 waypoint6)
    (can-traverse rover1 waypoint6 waypoint2)
    (can-traverse rover1 waypoint2 waypoint9)
    (can-traverse rover1 waypoint9 waypoint2)
    (can-traverse rover1 waypoint2 waypoint10)
    (can-traverse rover1 waypoint10 waypoint2)
    (can-traverse rover1 waypoint3 waypoint1)
    (can-traverse rover1 waypoint1 waypoint3)
    (can-traverse rover1 waypoint3 waypoint8)
    (can-traverse rover1 waypoint8 waypoint3)
    (can-traverse rover1 waypoint4 waypoint7)
    (can-traverse rover1 waypoint7 waypoint4)
    (can-traverse rover1 waypoint9 waypoint11)
    (can-traverse rover1 waypoint11 waypoint9)
    (can-traverse rover1 waypoint1 waypoint0)
    (can-traverse rover1 waypoint0 waypoint1)
    (at rover2 waypoint9)
    (available rover2)
    (store-of rover2store rover2)
    (empty rover2store)
    (equipped-for-rock-analysis rover2)
    (equipped-for-imaging rover2)
    (can-traverse rover2 waypoint9 waypoint0)
    (can-traverse rover2 waypoint0 waypoint9)
    (can-traverse rover2 waypoint9 waypoint1)
    (can-traverse rover2 waypoint1 waypoint9)
    (can-traverse rover2 waypoint9 waypoint2)
    (can-traverse rover2 waypoint2 waypoint9)
    (can-traverse rover2 waypoint9 waypoint3)
    (can-traverse rover2 waypoint3 waypoint9)
    (can-traverse rover2 waypoint9 waypoint4)
    (can-traverse rover2 waypoint4 waypoint9)
    (can-traverse rover2 waypoint0 waypoint7)
    (can-traverse rover2 waypoint7 waypoint0)
    (can-traverse rover2 waypoint0 waypoint8)
    (can-traverse rover2 waypoint8 waypoint0)
    (can-traverse rover2 waypoint0 waypoint10)
    (can-traverse rover2 waypoint10 waypoint0)
    (can-traverse rover2 waypoint0 waypoint11)
    (can-traverse rover2 waypoint11 waypoint0)
    (can-traverse rover2 waypoint2 waypoint5)
    (can-traverse rover2 waypoint5 waypoint2)
    (can-traverse rover2 waypoint2 waypoint6)
    (can-traverse rover2 waypoint6 waypoint2)
    (at rover3 waypoint0)
    (available rover3)
    (store-of rover3store rover3)
    (empty rover3store)
    (equipped-for-rock-analysis rover3)
    (can-traverse rover3 waypoint0 waypoint1)
    (can-traverse rover3 waypoint1 waypoint0)
    (can-traverse rover3 waypoint0 waypoint8)
    (can-traverse rover3 waypoint8 waypoint0)
    (can-traverse rover3 waypoint0 waypoint9)
    (can-traverse rover3 waypoint9 waypoint0)
    (can-traverse rover3 waypoint0 waypoint10)
    (can-traverse rover3 waypoint10 waypoint0)
    (can-traverse rover3 waypoint0 waypoint11)
    (can-traverse rover3 waypoint11 waypoint0)
    (can-traverse rover3 waypoint1 waypoint3)
    (can-traverse rover3 waypoint3 waypoint1)
    (can-traverse rover3 waypoint1 waypoint6)
    (can-traverse rover3 waypoint6 waypoint1)
    (can-traverse rover3 waypoint9 waypoint4)
    (can-traverse rover3 waypoint4 waypoint9)
    (can-traverse rover3 waypoint10 waypoint2)
    (can-traverse rover3 waypoint2 waypoint10)
    (can-traverse rover3 waypoint10 waypoint5)
    (can-traverse rover3 waypoint5 waypoint10)
    (can-traverse rover3 waypoint10 waypoint7)
    (can-traverse rover3 waypoint7 waypoint10)
    (on-board camera0 rover2)
    (calibration-target camera0 objective3)
    (supports camera0 low-res)
    (on-board camera1 rover2)
    (calibration-target camera1 objective4)
    (supports camera1 colour)
    (on-board camera2 rover0)
    (calibration-target camera2 objective2)
    (supports camera2 high-res)
    (on-board camera3 rover2)
    (calibration-target camera3 objective3)
    (supports camera3 colour)
    (supports camera3 high-res)
    (supports camera3 low-res)
    (visible-from objective0 waypoint0)
    (visible-from objective0 waypoint1)
    (visible-from objective0 waypoint2)
    (visible-from objective0 waypoint3)
    (visible-from objective0 waypoint4)
    (visible-from objective0 waypoint5)
    (visible-from objective0 waypoint6)
    (visible-from objective0 waypoint7)
    (visible-from objective0 waypoint8)
    (visible-from objective1 waypoint0)
    (visible-from objective1 waypoint1)
    (visible-from objective1 waypoint2)
    (visible-from objective1 waypoint3)
    (visible-from objective1 waypoint4)
    (visible-from objective1 waypoint5)
    (visible-from objective1 waypoint6)
    (visible-from objective2 waypoint0)
    (visible-from objective2 waypoint1)
    (visible-from objective2 waypoint2)
    (visible-from objective2 waypoint3)
    (visible-from objective2 waypoint4)
    (visible-from objective2 waypoint5)
    (visible-from objective2 waypoint6)
    (visible-from objective2 waypoint7)
    (visible-from objective2 waypoint8)
    (visible-from objective2 waypoint9)
    (visible-from objective3 waypoint0)
    (visible-from objective3 waypoint1)
    (visible-from objective4 waypoint0)
    (visible-from objective4 waypoint1)
    (visible-from objective4 waypoint2)
    (visible-from objective4 waypoint3)
    (visible-from objective4 waypoint4)
    (visible-from objective4 waypoint5)
    (visible-from objective4 waypoint6)
    (visible-from objective4 waypoint7)
    (visible-from objective4 waypoint8)
    (visible-from objective4 waypoint9)
    (visible-from objective4 waypoint10)
    (type-camera camera0)
    (type-camera camera1)
    (type-camera camera2)
    (type-camera camera3)
    (type-lander general)
    (type-mode colour)
    (type-mode high-res)
    (type-mode low-res)
    (type-object camera0)
    (type-object camera1)
    (type-object camera2)
    (type-object camera3)
    (type-object colour)
    (type-object general)
    (type-object high-res)
    (type-object low-res)
    (type-object objective0)
    (type-object objective1)
    (type-object objective2)
    (type-object objective3)
    (type-object objective4)
    (type-object rover0)
    (type-object rover0store)
    (type-object rover1)
    (type-object rover1store)
    (type-object rover2)
    (type-object rover2store)
    (type-object rover3)
    (type-object rover3store)
    (type-object waypoint0)
    (type-object waypoint1)
    (type-object waypoint10)
    (type-object waypoint11)
    (type-object waypoint2)
    (type-object waypoint3)
    (type-object waypoint4)
    (type-object waypoint5)
    (type-object waypoint6)
    (type-object waypoint7)
    (type-object waypoint8)
    (type-object waypoint9)
    (type-objective objective0)
    (type-objective objective1)
    (type-objective objective2)
    (type-objective objective3)
    (type-objective objective4)
    (type-rover rover0)
    (type-rover rover1)
    (type-rover rover2)
    (type-rover rover3)
    (type-sort-for-camera0 camera0)
    (type-sort-for-camera1 camera1)
    (type-sort-for-camera2 camera2)
    (type-sort-for-camera3 camera3)
    (type-sort-for-colour colour)
    (type-sort-for-general general)
    (type-sort-for-high-res high-res)
    (type-sort-for-low-res low-res)
    (type-sort-for-objective0 objective0)
    (type-sort-for-objective1 objective1)
    (type-sort-for-objective2 objective2)
    (type-sort-for-objective3 objective3)
    (type-sort-for-objective4 objective4)
    (type-sort-for-rover0 rover0)
    (type-sort-for-rover0store rover0store)
    (type-sort-for-rover1 rover1)
    (type-sort-for-rover1store rover1store)
    (type-sort-for-rover2 rover2)
    (type-sort-for-rover2store rover2store)
    (type-sort-for-rover3 rover3)
    (type-sort-for-rover3store rover3store)
    (type-sort-for-waypoint0 waypoint0)
    (type-sort-for-waypoint1 waypoint1)
    (type-sort-for-waypoint10 waypoint10)
    (type-sort-for-waypoint11 waypoint11)
    (type-sort-for-waypoint2 waypoint2)
    (type-sort-for-waypoint3 waypoint3)
    (type-sort-for-waypoint4 waypoint4)
    (type-sort-for-waypoint5 waypoint5)
    (type-sort-for-waypoint6 waypoint6)
    (type-sort-for-waypoint7 waypoint7)
    (type-sort-for-waypoint8 waypoint8)
    (type-sort-for-waypoint9 waypoint9)
    (type-store rover0store)
    (type-store rover1store)
    (type-store rover2store)
    (type-store rover3store)
    (type-waypoint waypoint0)
    (type-waypoint waypoint1)
    (type-waypoint waypoint10)
    (type-waypoint waypoint11)
    (type-waypoint waypoint2)
    (type-waypoint waypoint3)
    (type-waypoint waypoint4)
    (type-waypoint waypoint5)
    (type-waypoint waypoint6)
    (type-waypoint waypoint7)
    (type-waypoint waypoint8)
    (type-waypoint waypoint9)
  )
  ((x--top))
)
