(defdomain domain (
  (:operator (!drive ?v ?l1 ?l2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l1) (type_location ?l2)
      (at ?v ?l1) (road ?l1 ?l2)
    )
    ;; delete effects
    ((at ?v ?l1))
    ;; add effects
    ((at ?v ?l2))
  )
  (:operator (!noop ?v ?l2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l2)
      (at ?v ?l2)
    )
    ;; delete effects
    ()
    ;; add effects
    ()
  )
  (:operator (!pick_up ?v ?l ?p ?s1 ?s2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p) (type_capacity_number ?s1) (type_capacity_number ?s2)
      (at ?v ?l) (at ?p ?l) (capacity_predecessor ?s1 ?s2) (capacity ?v ?s2)
    )
    ;; delete effects
    ((at ?p ?l) (capacity ?v ?s2))
    ;; add effects
    ((in ?p ?v) (capacity ?v ?s1))
  )
  (:operator (!drop ?v ?l ?p ?s1 ?s2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p) (type_capacity_number ?s1) (type_capacity_number ?s2)
      (at ?v ?l) (in ?p ?v) (capacity_predecessor ?s1 ?s2) (capacity ?v ?s1)
    )
    ;; delete effects
    ((in ?p ?v) (capacity ?v ?s1))
    ;; add effects
    ((at ?p ?l) (capacity ?v ?s2))
  )
  ;; method named __top_method
  (:method (__top)
    (
      
      (type_sort_for_city_loc_1 ?var_for_city_loc_1_1) (type_sort_for_package_0 ?var_for_package_0_2) (type_sort_for_city_loc_2 ?var_for_city_loc_2_3) (type_sort_for_package_1 ?var_for_package_1_4) (type_sort_for_city_loc_4 ?var_for_city_loc_4_5) (type_sort_for_package_2 ?var_for_package_2_6) (type_sort_for_city_loc_1 ?var_for_city_loc_1_7) (type_sort_for_package_3 ?var_for_package_3_8) (type_sort_for_city_loc_2 ?var_for_city_loc_2_9) (type_sort_for_package_4 ?var_for_package_4_10) (type_sort_for_city_loc_1 ?var_for_city_loc_1_11) (type_sort_for_package_5 ?var_for_package_5_12) (type_sort_for_city_loc_4 ?var_for_city_loc_4_13) (type_sort_for_package_6 ?var_for_package_6_14)
      
    )
    ((deliver ?var_for_package_0_2 ?var_for_city_loc_1_1) (deliver ?var_for_package_4_10 ?var_for_city_loc_2_9) (deliver ?var_for_package_3_8 ?var_for_city_loc_1_7) (deliver ?var_for_package_2_6 ?var_for_city_loc_4_5) (deliver ?var_for_package_5_12 ?var_for_city_loc_1_11) (deliver ?var_for_package_1_4 ?var_for_city_loc_2_3) (deliver ?var_for_package_6_14 ?var_for_city_loc_4_13))
  )
  ;; method named m_deliver_ordering_0
  (:method (deliver ?p ?l2)
    (
      (type_package ?p) (type_location ?l2)
      (type_location ?l1) (type_location ?l2) (type_package ?p) (type_vehicle ?v)
      
    )
    ((get_to ?v ?l1) (load ?v ?l1 ?p) (get_to ?v ?l2) (unload ?v ?l2 ?p))
  )
  ;; method named m_drive_to_ordering_0
  (:method (get_to ?v ?l2)
    (
      (type_vehicle ?v) (type_location ?l2)
      (type_location ?l1) (type_location ?l2) (type_vehicle ?v)
      
    )
    ((!drive ?v ?l1 ?l2))
  )
  ;; method named m_drive_to_via_ordering_0
  (:method (get_to ?v ?l3)
    (
      (type_vehicle ?v) (type_location ?l3)
      (type_location ?l2) (type_location ?l3) (type_vehicle ?v)
      
    )
    ((get_to ?v ?l2) (!drive ?v ?l2 ?l3))
  )
  ;; method named m_i_am_there_ordering_0
  (:method (get_to ?v ?l)
    (
      (type_vehicle ?v) (type_location ?l)
      (type_location ?l) (type_vehicle ?v)
      
    )
    ((!noop ?v ?l))
  )
  ;; method named m_load_ordering_0
  (:method (load ?v ?l ?p)
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p)
      (type_location ?l) (type_package ?p) (type_capacity_number ?s1) (type_capacity_number ?s2) (type_vehicle ?v)
      
    )
    ((!pick_up ?v ?l ?p ?s1 ?s2))
  )
  ;; method named m_unload_ordering_0
  (:method (unload ?v ?l ?p)
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p)
      (type_location ?l) (type_package ?p) (type_capacity_number ?s1) (type_capacity_number ?s2) (type_vehicle ?v)
      
    )
    ((!drop ?v ?l ?p ?s1 ?s2))
  )
))
