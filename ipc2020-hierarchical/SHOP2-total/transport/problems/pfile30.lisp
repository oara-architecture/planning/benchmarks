(defproblem problem domain 
  (
    (capacity_predecessor capacity_0 capacity_1)
    (capacity_predecessor capacity_1 capacity_2)
    (capacity_predecessor capacity_2 capacity_3)
    (road city_loc_0 city_loc_16)
    (road city_loc_0 city_loc_2)
    (road city_loc_0 city_loc_20)
    (road city_loc_0 city_loc_22)
    (road city_loc_1 city_loc_29)
    (road city_loc_2 city_loc_0)
    (road city_loc_2 city_loc_5)
    (road city_loc_2 city_loc_7)
    (road city_loc_3 city_loc_3)
    (road city_loc_3 city_loc_21)
    (road city_loc_3 city_loc_7)
    (road city_loc_3 city_loc_26)
    (road city_loc_3 city_loc_13)
    (road city_loc_4 city_loc_4)
    (road city_loc_4 city_loc_5)
    (road city_loc_4 city_loc_7)
    (road city_loc_4 city_loc_25)
    (road city_loc_4 city_loc_14)
    (road city_loc_4 city_loc_15)
    (road city_loc_5 city_loc_2)
    (road city_loc_5 city_loc_18)
    (road city_loc_5 city_loc_19)
    (road city_loc_5 city_loc_4)
    (road city_loc_5 city_loc_15)
    (road city_loc_6 city_loc_8)
    (road city_loc_6 city_loc_24)
    (road city_loc_6 city_loc_26)
    (road city_loc_7 city_loc_2)
    (road city_loc_7 city_loc_19)
    (road city_loc_7 city_loc_3)
    (road city_loc_7 city_loc_20)
    (road city_loc_7 city_loc_4)
    (road city_loc_7 city_loc_12)
    (road city_loc_8 city_loc_16)
    (road city_loc_8 city_loc_6)
    (road city_loc_8 city_loc_24)
    (road city_loc_8 city_loc_26)
    (road city_loc_9 city_loc_21)
    (road city_loc_9 city_loc_26)
    (road city_loc_9 city_loc_11)
    (road city_loc_9 city_loc_13)
    (road city_loc_10 city_loc_27)
    (road city_loc_10 city_loc_29)
    (road city_loc_11 city_loc_20)
    (road city_loc_11 city_loc_22)
    (road city_loc_11 city_loc_9)
    (road city_loc_12 city_loc_7)
    (road city_loc_12 city_loc_28)
    (road city_loc_13 city_loc_3)
    (road city_loc_13 city_loc_20)
    (road city_loc_13 city_loc_9)
    (road city_loc_13 city_loc_26)
    (road city_loc_14 city_loc_18)
    (road city_loc_14 city_loc_4)
    (road city_loc_14 city_loc_21)
    (road city_loc_14 city_loc_26)
    (road city_loc_15 city_loc_4)
    (road city_loc_15 city_loc_5)
    (road city_loc_16 city_loc_0)
    (road city_loc_16 city_loc_17)
    (road city_loc_16 city_loc_22)
    (road city_loc_16 city_loc_8)
    (road city_loc_16 city_loc_28)
    (road city_loc_17 city_loc_16)
    (road city_loc_17 city_loc_19)
    (road city_loc_17 city_loc_20)
    (road city_loc_17 city_loc_26)
    (road city_loc_17 city_loc_27)
    (road city_loc_18 city_loc_20)
    (road city_loc_18 city_loc_5)
    (road city_loc_18 city_loc_23)
    (road city_loc_18 city_loc_26)
    (road city_loc_18 city_loc_14)
    (road city_loc_19 city_loc_17)
    (road city_loc_19 city_loc_19)
    (road city_loc_19 city_loc_5)
    (road city_loc_19 city_loc_7)
    (road city_loc_19 city_loc_25)
    (road city_loc_20 city_loc_0)
    (road city_loc_20 city_loc_17)
    (road city_loc_20 city_loc_18)
    (road city_loc_20 city_loc_7)
    (road city_loc_20 city_loc_11)
    (road city_loc_20 city_loc_13)
    (road city_loc_21 city_loc_3)
    (road city_loc_21 city_loc_24)
    (road city_loc_21 city_loc_9)
    (road city_loc_21 city_loc_26)
    (road city_loc_21 city_loc_14)
    (road city_loc_22 city_loc_16)
    (road city_loc_22 city_loc_0)
    (road city_loc_22 city_loc_27)
    (road city_loc_22 city_loc_11)
    (road city_loc_23 city_loc_18)
    (road city_loc_24 city_loc_21)
    (road city_loc_24 city_loc_6)
    (road city_loc_24 city_loc_8)
    (road city_loc_24 city_loc_25)
    (road city_loc_25 city_loc_19)
    (road city_loc_25 city_loc_4)
    (road city_loc_25 city_loc_24)
    (road city_loc_26 city_loc_17)
    (road city_loc_26 city_loc_18)
    (road city_loc_26 city_loc_3)
    (road city_loc_26 city_loc_21)
    (road city_loc_26 city_loc_6)
    (road city_loc_26 city_loc_8)
    (road city_loc_26 city_loc_9)
    (road city_loc_26 city_loc_13)
    (road city_loc_26 city_loc_14)
    (road city_loc_27 city_loc_17)
    (road city_loc_27 city_loc_22)
    (road city_loc_27 city_loc_10)
    (road city_loc_27 city_loc_27)
    (road city_loc_28 city_loc_16)
    (road city_loc_28 city_loc_12)
    (road city_loc_29 city_loc_1)
    (road city_loc_29 city_loc_10)
    (at package_0 city_loc_11)
    (at package_1 city_loc_20)
    (at package_2 city_loc_14)
    (at package_3 city_loc_26)
    (at package_4 city_loc_7)
    (at package_5 city_loc_8)
    (at package_6 city_loc_7)
    (at package_7 city_loc_27)
    (at package_8 city_loc_3)
    (at package_9 city_loc_24)
    (at package_10 city_loc_12)
    (at package_11 city_loc_0)
    (at package_12 city_loc_16)
    (at package_13 city_loc_24)
    (at package_14 city_loc_23)
    (at package_15 city_loc_12)
    (at package_16 city_loc_18)
    (at package_17 city_loc_29)
    (at package_18 city_loc_2)
    (at package_19 city_loc_4)
    (at truck_0 city_loc_3)
    (at truck_1 city_loc_17)
    (at truck_2 city_loc_13)
    (at truck_3 city_loc_24)
    (at truck_4 city_loc_11)
    (at truck_5 city_loc_1)
    (capacity truck_0 capacity_3)
    (capacity truck_1 capacity_3)
    (capacity truck_2 capacity_3)
    (capacity truck_3 capacity_3)
    (capacity truck_4 capacity_3)
    (capacity truck_5 capacity_3)
    (type_capacity_number capacity_0)
    (type_capacity_number capacity_1)
    (type_capacity_number capacity_2)
    (type_capacity_number capacity_3)
    (type_locatable package_0)
    (type_locatable package_1)
    (type_locatable package_10)
    (type_locatable package_11)
    (type_locatable package_12)
    (type_locatable package_13)
    (type_locatable package_14)
    (type_locatable package_15)
    (type_locatable package_16)
    (type_locatable package_17)
    (type_locatable package_18)
    (type_locatable package_19)
    (type_locatable package_2)
    (type_locatable package_3)
    (type_locatable package_4)
    (type_locatable package_5)
    (type_locatable package_6)
    (type_locatable package_7)
    (type_locatable package_8)
    (type_locatable package_9)
    (type_locatable truck_0)
    (type_locatable truck_1)
    (type_locatable truck_2)
    (type_locatable truck_3)
    (type_locatable truck_4)
    (type_locatable truck_5)
    (type_location city_loc_0)
    (type_location city_loc_1)
    (type_location city_loc_10)
    (type_location city_loc_11)
    (type_location city_loc_12)
    (type_location city_loc_13)
    (type_location city_loc_14)
    (type_location city_loc_15)
    (type_location city_loc_16)
    (type_location city_loc_17)
    (type_location city_loc_18)
    (type_location city_loc_19)
    (type_location city_loc_2)
    (type_location city_loc_20)
    (type_location city_loc_21)
    (type_location city_loc_22)
    (type_location city_loc_23)
    (type_location city_loc_24)
    (type_location city_loc_25)
    (type_location city_loc_26)
    (type_location city_loc_27)
    (type_location city_loc_28)
    (type_location city_loc_29)
    (type_location city_loc_3)
    (type_location city_loc_4)
    (type_location city_loc_5)
    (type_location city_loc_6)
    (type_location city_loc_7)
    (type_location city_loc_8)
    (type_location city_loc_9)
    (type_object capacity_0)
    (type_object capacity_1)
    (type_object capacity_2)
    (type_object capacity_3)
    (type_object city_loc_0)
    (type_object city_loc_1)
    (type_object city_loc_10)
    (type_object city_loc_11)
    (type_object city_loc_12)
    (type_object city_loc_13)
    (type_object city_loc_14)
    (type_object city_loc_15)
    (type_object city_loc_16)
    (type_object city_loc_17)
    (type_object city_loc_18)
    (type_object city_loc_19)
    (type_object city_loc_2)
    (type_object city_loc_20)
    (type_object city_loc_21)
    (type_object city_loc_22)
    (type_object city_loc_23)
    (type_object city_loc_24)
    (type_object city_loc_25)
    (type_object city_loc_26)
    (type_object city_loc_27)
    (type_object city_loc_28)
    (type_object city_loc_29)
    (type_object city_loc_3)
    (type_object city_loc_4)
    (type_object city_loc_5)
    (type_object city_loc_6)
    (type_object city_loc_7)
    (type_object city_loc_8)
    (type_object city_loc_9)
    (type_object package_0)
    (type_object package_1)
    (type_object package_10)
    (type_object package_11)
    (type_object package_12)
    (type_object package_13)
    (type_object package_14)
    (type_object package_15)
    (type_object package_16)
    (type_object package_17)
    (type_object package_18)
    (type_object package_19)
    (type_object package_2)
    (type_object package_3)
    (type_object package_4)
    (type_object package_5)
    (type_object package_6)
    (type_object package_7)
    (type_object package_8)
    (type_object package_9)
    (type_object truck_0)
    (type_object truck_1)
    (type_object truck_2)
    (type_object truck_3)
    (type_object truck_4)
    (type_object truck_5)
    (type_package package_0)
    (type_package package_1)
    (type_package package_10)
    (type_package package_11)
    (type_package package_12)
    (type_package package_13)
    (type_package package_14)
    (type_package package_15)
    (type_package package_16)
    (type_package package_17)
    (type_package package_18)
    (type_package package_19)
    (type_package package_2)
    (type_package package_3)
    (type_package package_4)
    (type_package package_5)
    (type_package package_6)
    (type_package package_7)
    (type_package package_8)
    (type_package package_9)
    (type_sort_for_capacity_0 capacity_0)
    (type_sort_for_capacity_1 capacity_1)
    (type_sort_for_capacity_2 capacity_2)
    (type_sort_for_capacity_3 capacity_3)
    (type_sort_for_city_loc_0 city_loc_0)
    (type_sort_for_city_loc_1 city_loc_1)
    (type_sort_for_city_loc_10 city_loc_10)
    (type_sort_for_city_loc_11 city_loc_11)
    (type_sort_for_city_loc_12 city_loc_12)
    (type_sort_for_city_loc_13 city_loc_13)
    (type_sort_for_city_loc_14 city_loc_14)
    (type_sort_for_city_loc_15 city_loc_15)
    (type_sort_for_city_loc_16 city_loc_16)
    (type_sort_for_city_loc_17 city_loc_17)
    (type_sort_for_city_loc_18 city_loc_18)
    (type_sort_for_city_loc_19 city_loc_19)
    (type_sort_for_city_loc_2 city_loc_2)
    (type_sort_for_city_loc_20 city_loc_20)
    (type_sort_for_city_loc_21 city_loc_21)
    (type_sort_for_city_loc_22 city_loc_22)
    (type_sort_for_city_loc_23 city_loc_23)
    (type_sort_for_city_loc_24 city_loc_24)
    (type_sort_for_city_loc_25 city_loc_25)
    (type_sort_for_city_loc_26 city_loc_26)
    (type_sort_for_city_loc_27 city_loc_27)
    (type_sort_for_city_loc_28 city_loc_28)
    (type_sort_for_city_loc_29 city_loc_29)
    (type_sort_for_city_loc_3 city_loc_3)
    (type_sort_for_city_loc_4 city_loc_4)
    (type_sort_for_city_loc_5 city_loc_5)
    (type_sort_for_city_loc_6 city_loc_6)
    (type_sort_for_city_loc_7 city_loc_7)
    (type_sort_for_city_loc_8 city_loc_8)
    (type_sort_for_city_loc_9 city_loc_9)
    (type_sort_for_package_0 package_0)
    (type_sort_for_package_1 package_1)
    (type_sort_for_package_10 package_10)
    (type_sort_for_package_11 package_11)
    (type_sort_for_package_12 package_12)
    (type_sort_for_package_13 package_13)
    (type_sort_for_package_14 package_14)
    (type_sort_for_package_15 package_15)
    (type_sort_for_package_16 package_16)
    (type_sort_for_package_17 package_17)
    (type_sort_for_package_18 package_18)
    (type_sort_for_package_19 package_19)
    (type_sort_for_package_2 package_2)
    (type_sort_for_package_3 package_3)
    (type_sort_for_package_4 package_4)
    (type_sort_for_package_5 package_5)
    (type_sort_for_package_6 package_6)
    (type_sort_for_package_7 package_7)
    (type_sort_for_package_8 package_8)
    (type_sort_for_package_9 package_9)
    (type_sort_for_truck_0 truck_0)
    (type_sort_for_truck_1 truck_1)
    (type_sort_for_truck_2 truck_2)
    (type_sort_for_truck_3 truck_3)
    (type_sort_for_truck_4 truck_4)
    (type_sort_for_truck_5 truck_5)
    (type_vehicle truck_0)
    (type_vehicle truck_1)
    (type_vehicle truck_2)
    (type_vehicle truck_3)
    (type_vehicle truck_4)
    (type_vehicle truck_5)
  )
  ((__top))
)
