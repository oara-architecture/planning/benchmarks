(defdomain domain (
  (:operator (!drive ?v ?l1 ?l2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l1) (type_location ?l2)
      (at ?v ?l1) (road ?l1 ?l2)
    )
    ;; delete effects
    ((at ?v ?l1))
    ;; add effects
    ((at ?v ?l2))
  )
  (:operator (!noop ?v ?l2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l2)
      (at ?v ?l2)
    )
    ;; delete effects
    ()
    ;; add effects
    ()
  )
  (:operator (!pick-up ?v ?l ?p ?s1 ?s2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p) (type_capacity-number ?s1) (type_capacity-number ?s2)
      (at ?v ?l) (at ?p ?l) (capacity-predecessor ?s1 ?s2) (capacity ?v ?s2)
    )
    ;; delete effects
    ((at ?p ?l) (capacity ?v ?s2))
    ;; add effects
    ((in ?p ?v) (capacity ?v ?s1))
  )
  (:operator (!drop ?v ?l ?p ?s1 ?s2)
    ;; preconditions
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p) (type_capacity-number ?s1) (type_capacity-number ?s2)
      (at ?v ?l) (in ?p ?v) (capacity-predecessor ?s1 ?s2) (capacity ?v ?s1)
    )
    ;; delete effects
    ((in ?p ?v) (capacity ?v ?s1))
    ;; add effects
    ((at ?p ?l) (capacity ?v ?s2))
  )
  ;; method named __top_method
  (:method (__top)
    (
      
      (type_sort_for_city-loc-3 ?var_for_city-loc-3_1) (type_sort_for_package-0 ?var_for_package-0_2) (type_sort_for_city-loc-0 ?var_for_city-loc-0_3) (type_sort_for_package-1 ?var_for_package-1_4) (type_sort_for_city-loc-11 ?var_for_city-loc-11_5) (type_sort_for_package-2 ?var_for_package-2_6) (type_sort_for_city-loc-0 ?var_for_city-loc-0_7) (type_sort_for_package-3 ?var_for_package-3_8) (type_sort_for_city-loc-4 ?var_for_city-loc-4_9) (type_sort_for_package-4 ?var_for_package-4_10) (type_sort_for_city-loc-0 ?var_for_city-loc-0_11) (type_sort_for_package-5 ?var_for_package-5_12) (type_sort_for_city-loc-5 ?var_for_city-loc-5_13) (type_sort_for_package-6 ?var_for_package-6_14) (type_sort_for_city-loc-9 ?var_for_city-loc-9_15) (type_sort_for_package-7 ?var_for_package-7_16) (type_sort_for_city-loc-5 ?var_for_city-loc-5_17) (type_sort_for_package-8 ?var_for_package-8_18) (type_sort_for_city-loc-6 ?var_for_city-loc-6_19) (type_sort_for_package-9 ?var_for_package-9_20) (type_sort_for_city-loc-5 ?var_for_city-loc-5_21) (type_sort_for_package-10 ?var_for_package-10_22) (type_sort_for_city-loc-3 ?var_for_city-loc-3_23) (type_sort_for_package-11 ?var_for_package-11_24) (type_sort_for_city-loc-11 ?var_for_city-loc-11_25) (type_sort_for_package-12 ?var_for_package-12_26) (type_sort_for_city-loc-2 ?var_for_city-loc-2_27) (type_sort_for_package-13 ?var_for_package-13_28) (type_sort_for_city-loc-10 ?var_for_city-loc-10_29) (type_sort_for_package-14 ?var_for_package-14_30) (type_sort_for_city-loc-11 ?var_for_city-loc-11_31) (type_sort_for_package-15 ?var_for_package-15_32)
      
    )
    (:unordered (deliver ?var_for_package-0_2 ?var_for_city-loc-3_1) (deliver ?var_for_package-1_4 ?var_for_city-loc-0_3) (deliver ?var_for_package-2_6 ?var_for_city-loc-11_5) (deliver ?var_for_package-3_8 ?var_for_city-loc-0_7) (deliver ?var_for_package-4_10 ?var_for_city-loc-4_9) (deliver ?var_for_package-5_12 ?var_for_city-loc-0_11) (deliver ?var_for_package-6_14 ?var_for_city-loc-5_13) (deliver ?var_for_package-7_16 ?var_for_city-loc-9_15) (deliver ?var_for_package-8_18 ?var_for_city-loc-5_17) (deliver ?var_for_package-9_20 ?var_for_city-loc-6_19) (deliver ?var_for_package-10_22 ?var_for_city-loc-5_21) (deliver ?var_for_package-11_24 ?var_for_city-loc-3_23) (deliver ?var_for_package-12_26 ?var_for_city-loc-11_25) (deliver ?var_for_package-13_28 ?var_for_city-loc-2_27) (deliver ?var_for_package-14_30 ?var_for_city-loc-10_29) (deliver ?var_for_package-15_32 ?var_for_city-loc-11_31))
  )
  ;; method named m-deliver
  (:method (deliver ?p ?l2)
    (
      (type_package ?p) (type_location ?l2)
      (type_package ?p) (type_location ?l1) (type_location ?l2) (type_vehicle ?v)
      
    )
    ((get-to ?v ?l1) (load ?v ?l1 ?p) (get-to ?v ?l2) (unload ?v ?l2 ?p))
  )
  ;; method named m-drive-to
  (:method (get-to ?v ?l2)
    (
      (type_vehicle ?v) (type_location ?l2)
      (type_vehicle ?v) (type_location ?l1) (type_location ?l2)
      
    )
    ((!drive ?v ?l1 ?l2))
  )
  ;; method named m-drive-to-via
  (:method (get-to ?v ?l3)
    (
      (type_vehicle ?v) (type_location ?l3)
      (type_vehicle ?v) (type_location ?l2) (type_location ?l3)
      
    )
    ((get-to ?v ?l2) (!drive ?v ?l2 ?l3))
  )
  ;; method named m-i-am-there
  (:method (get-to ?v ?l)
    (
      (type_vehicle ?v) (type_location ?l)
      (type_vehicle ?v) (type_location ?l)
      
    )
    ((!noop ?v ?l))
  )
  ;; method named m-load
  (:method (load ?v ?l ?p)
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p)
      (type_vehicle ?v) (type_location ?l) (type_package ?p) (type_capacity-number ?s1) (type_capacity-number ?s2)
      
    )
    ((!pick-up ?v ?l ?p ?s1 ?s2))
  )
  ;; method named m-unload
  (:method (unload ?v ?l ?p)
    (
      (type_vehicle ?v) (type_location ?l) (type_package ?p)
      (type_vehicle ?v) (type_location ?l) (type_package ?p) (type_capacity-number ?s1) (type_capacity-number ?s2)
      
    )
    ((!drop ?v ?l ?p ?s1 ?s2))
  )
))
